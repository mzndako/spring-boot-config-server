package com.example.configserver;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.SpringApplication;
//import org.springframework.boot.RequestMapping;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.cloud.config.server.EnableConfigServer;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.mvc.method.annotation.RequestMappingHandlerMapping;
import org.springframework.web.servlet.mvc.method.RequestMappingInfoHandlerMapping;

@SpringBootApplication
@EnableAutoConfiguration
@EnableConfigServer
@ComponentScan
@RequestMapping
@RestController
@Configuration
@EnableWebSecurity
public class ConfigServerApplication {


    //@RequestMapping("/encrypt")
    //public String home() {
    //    return "Hello World!";
    //}


	public static void main(String[] args) throws Exception {
		CheckJce.validate();
		SpringApplication.run(ConfigServerApplication.class, args);
	}
}
